"use strict";

/////////////////////////////////////////////////////////////////////////////////////////
//
// cs411 assignment 1 (Fall 2016) - raster graphics
//
/////////////////////////////////////////////////////////////////////////////////////////


var ctx;
var imageData;

var pauseFlag=1;
var lineFlag=0;
var triangleFlag=0;
var fillFlag=1;
var randomFlag=0;

function togglePause() {
  pauseFlag= 1- pauseFlag;
  console.log('pauseFlag = %d', pauseFlag);
}

function toggleLine() {
  lineFlag= 1- lineFlag;
  console.log('lineFlag = %d', lineFlag);
}

function toggleTriangle() {
  triangleFlag= 1- triangleFlag;
  console.log('triangleFlag = %d', triangleFlag);
}

function toggleFill() {
  fillFlag= 1- fillFlag;
  console.log('fillFlag = %d', fillFlag);
}

function toggleRandomColor(){
	randomFlag = 1-randomFlag;
	console.log('randomFlag = %d', randomFlag);
}

function animate() 
{
  if(!pauseFlag) {
    if (lineFlag) drawRandomLineSegment();
    if (triangleFlag) drawRandomTriangle();
  }
  setTimeout(animate,100); // call animate() in 1000 msec
} 


function initImage(img) 
{
  var canvas = document.getElementById('mycanvas');
  ctx = canvas.getContext('2d');

  ctx.drawImage(img, 0, 0);
  imageData = ctx.getImageData(0,0,canvas.width, canvas.height); // get reference to image data
}


function main()
{

	
  // load and display image
  var img = new Image();
  img.src = 'data/frac2.png';
  img.onload = function() { initImage(this);}

  // set button listeners
  var grayscalebtn = document.getElementById('grayscaleButton');
  grayscalebtn.addEventListener('click', grayscale);

  var pausebtn = document.getElementById('pauseButton');
  pausebtn.addEventListener('click', togglePause);

  var linebtn = document.getElementById('lineButton');
  linebtn.addEventListener('click', toggleLine);

  var trianglebtn = document.getElementById('triangleButton');
  trianglebtn.addEventListener('click', toggleTriangle);

  var fillbtn = document.getElementById('fillButton');
  fillbtn.addEventListener('click', toggleFill);

	var randomcolorbtn = document.getElementById('randomColorButton');
	randomcolorbtn.addEventListener('click', toggleRandomColor);

  // start animation
  animate();
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// conversion to grayscale
// 
/////////////////////////////////////////////////////////////////////////////////////////

function grayscale() 
{
  var data = imageData.data;
  for (var i = 0; i < data.length; i += 4) {
    var m = (data[i] + data[i +1] + data[i +2]) / 3;
    data[i]     = m; // red
    data[i + 1] = m; // green
    data[i + 2] = m; // blue
  }
  ctx.putImageData(imageData, 0, 0);
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// draw lines
//
/////////////////////////////////////////////////////////////////////////////////////////


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// REPLACE THIS WITH YOUR FUNCTION FOLLOWING THE ASSIGNMENT SPECIFICATIONS
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//MY FUNCTION
function drawLineSegment(vs,ve,color)
{
	
	var data = imageData.data;
	var h = imageData.height;
	var w = imageData.width;
	
	//IGNORE ILLEGAL LINES
	if ((vs[0] <0) || (vs[1] <0) || (ve[0] >= w) || (ve[1] >= h)) return;
	if ((vs[0] == ve[0]) && (vs[1] == ve[1])) return;
	
	
	//SUBMISSION
	//PROGRAMMER: Patrick Mendus

	var dx = Math.abs(ve[0]-vs[0]); 
	var dy = Math.abs(ve[1]-vs[1]);
  
	var xk = vs[0];
	var yk = vs[1];
  
	var xdirection = 1;
	var ydirection = 1;
	var swapped = false; 

	if ((ve[0] - vs[0]) < 0){
		xdirection = -1;
	}
	if ((ve[1] - vs[1]) < 0){
		ydirection = -1;	
	}
	
	if (dy > dx){
		var temp = dy;
		dy = dx;
		dx = temp;
		swapped = true;	
	}
	
	var Pk = 2*dy - dx;
	var xk = vs[0];
	var yk = vs[1];
	
	for (var i = 0; i < dx; i++){
		data[(yk*w+xk)*4+0]     = color[0]; // red
		data[(yk*w+xk)*4+1]     = color[1]; // green
		data[(yk*w+xk)*4+2]     = color[2]; // blue
		
		while (Pk >=0){
			if (swapped){
				xk = xk + 1;	
			} else {
				yk = yk + 1;	
			}
			
			Pk = Pk - (2*dx);
		}
		
		if (swapped){
			yk = yk + ydirection;	
		} else {
			xk = xk + xdirection;	
		}
		
		Pk = Pk + (2*dy);
	}

	// update image
	ctx.putImageData(imageData, 0, 0);
}


//ORIGINAL FUNCTION
/*
function drawLineSegment(vs,ve,color)
{
			console.log("DRAW: (" + vs[0] + "," + vs[1] + "), (" + ve[0] + "," + ve[1] + ")");
  var data = imageData.data;
  var h = imageData.height;
  var w = imageData.width;

  var dx = ve[0] -vs[0]; 
  var dy = ve[1] -vs[1]; 
  var m = dy/dx;             // slope 
  var b = vs[1]-m*vs[0];     // y-intercept

  // ignore illegal lines
  if ((vs[0] <0) || (vs[1] <0) || (ve[0] >= w) || (ve[1] >= h)) return;
  if ((vs[0] == ve[0]) && (vs[1] == ve[1])) return;

  // handle nearly horizontal lines
  if(Math.abs(m)<1){
    for (var x = Math.min(vs[0],ve[0]); x <= Math.max(vs[0],ve[0]); x++) {
      var y=Math.round(m*x+b); // compute y coordinate
      var yi=h-y;//invert y coordinate
      data[(yi*w+x)*4+0]     = color[0]; // red
      data[(yi*w+x)*4+1]     = color[1]; // green
      data[(yi*w+x)*4+2]     = color[2]; // blue
    }    
  }

  // handle nearly vertical lines
  else {
    for (var y = Math.min(vs[1],ve[1]); y <= Math.max(vs[1],ve[1]); y++) {
      var x=Math.round((y-b)/m); // compute y coordinate
      var yi=h-y;//invert y coordinate
      data[(yi*w+x)*4+0]     = color[0]; // red
      data[(yi*w+x)*4+1]     = color[1]; // green
      data[(yi*w+x)*4+2]     = color[2]; // blue
    }    
  }

  // update image
  ctx.putImageData(imageData, 0, 0);
}
*/

function drawRandomLineSegment()
{
  var h = imageData.height;
  var w = imageData.width;
  
  var xs=Math.floor(Math.random()*w);
  var ys=Math.floor(Math.random()*h);
  var xe=Math.floor(Math.random()*w);
  var ye=Math.floor(Math.random()*h);
  var r=Math.floor(Math.random()*255);
  var g=Math.floor(Math.random()*255);
  var b=Math.floor(Math.random()*255);

  drawLineSegment([xs,ys] ,[xe,ye],[r,g,b]);
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// draw triangles
//
/////////////////////////////////////////////////////////////////////////////////////////


function triangleArea(a,b,c)
{
  var area = ((b[1] - c[1]) * (a[0] - c[0]) + (c[0] - b[0]) * (a[1] - c[1]));
  area = Math.abs(0.5*area);
  return area;
}

function vertexInside(v,v0,v1,v2)
{
  var T = triangleArea(v0,v1,v2);

  var alpha = triangleArea(v,v0,v1) /T ;
  var beta  = triangleArea(v,v1,v2) /T ;
  var gamma = triangleArea(v,v2,v0) /T ;

  if ((alpha>=0) && (beta>=0) && (gamma>=0) && (Math.abs(alpha+beta+gamma -1)<0.00001)) return true;
  else return false;
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// REPLACE THIS WITH YOUR FUNCTION FOLLOWING THE ASSIGNMENT SPECIFICATIONS
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
function drawTriangle(v0,v1,v2,color)
{
	var data = imageData.data;
	var h = imageData.height;
	var w = imageData.width;

	if ((v0[0] == v1[0]) && (v0[1] == v1[1])) return;
	if ((v1[0] == v2[0]) && (v1[1] == v2[1])) return;
	if ((v2[0] == v0[0]) && (v2[1] == v0[1])) return;

 
  if (!fillFlag){
    drawLineSegment(v0 ,v1, color);
    drawLineSegment(v1 ,v2, color);
    drawLineSegment(v2 ,v0, color);
  }
	else {

		var x = v0[0] + ((v1[1]-v0[1])/(v2[1]-v0[1])) * (v2[0]-v0[0]);
		var v3 = [Math.floor(x), v1[1]];

		fillFirstTriangle(v0, v1, v3, color);
		fillSecondTriangle(v1, v3, v2, color);
	}

 //OLD DRAW TRIANGLE ALGORITHM
 /*
  // handle filled triangles
  else
  {
    var xmin = Math.min(v0[0],v1[0],v2[0]);
    var xmax = Math.max(v0[0],v1[0],v2[0]);
    var ymin = Math.min(v0[1],v1[1],v2[1]);
    var ymax = Math.max(v0[1],v1[1],v2[1]);

	for (var y=ymin; y<=ymax; y++) {
		
		if (randomFlag){
			  color = randomColor();	
		}
		
		for (var x=xmin; x<=xmax; x++){
			if (vertexInside([x,y],v0,v1,v2)){
			var yi=h-y;//invert y coordinate
        	data[(yi*w+x)*4+0]     = color[0]; // red
        	data[(yi*w+x)*4+1]     = color[1]; // green
        	data[(yi*w+x)*4+2]     = color[2]; // blue
			}
		}
    }
  }
  */


  ctx.putImageData(imageData, 0, 0);

}

function fillFirstTriangle(v0, v1, v2, color){

	var slope1 = (v1[0]-v0[0])/(v1[1]-v0[1]);
	var slope2 = (v2[0]-v0[0])/(v2[1]-v0[1]);
	
	var x1 = v1[0];
	var x2 = v2[0];

	if (randomFlag){
		for (var y = v1[1]; y <= v0[1]; y++){
			drawLineSegment([Math.floor(x1), y], [Math.floor(x2), y], randomColor());
			x1 += slope1;
			x2 += slope2;
		}
	} else {
		for (var y = v1[1]; y <= v0[1]; y++){
			drawLineSegment([Math.floor(x1), y], [Math.floor(x2), y], color);
			x1 += slope1;
			x2 += slope2;
		}		
	}
}

function fillSecondTriangle(v0, v1, v2, color){

	var slope1 = (v2[0]-v0[0])/(v2[1]-v0[1]);
	var slope2 = (v2[0]-v1[0])/(v2[1]-v1[1]);
	
	var x1 = v0[0];
	var x2 = v1[0];

	if (randomFlag){
		for (var y = v0[1]; y > v2[1]; y--){
				drawLineSegment([Math.floor(x1), y], [Math.floor(x2), y], randomColor());
				x1 -= slope1;
				x2 -= slope2;
		}
	} else {
		for (var y = v0[1]; y > v2[1]; y--){
				drawLineSegment([Math.floor(x1), y], [Math.floor(x2), y], color);
				x1 -= slope1;
				x2 -= slope2;
		}		
	}
}

function randomColor(){
  var r=Math.floor(Math.random()*255);
  var g=Math.floor(Math.random()*255);
  var b=Math.floor(Math.random()*255);
  return [r, g, b];	
}


function drawRandomTriangle()
{
  var h = imageData.height;
  var w = imageData.width;
  
  var v0x=Math.floor(Math.random()*w);
  var v0y=Math.floor(Math.random()*h);
  var v1x=Math.floor(Math.random()*w);
  var v1y=Math.floor(Math.random()*h);
  var v2x=Math.floor(Math.random()*w);
  var v2y=Math.floor(Math.random()*h);
  var r=Math.floor(Math.random()*255);
  var g=Math.floor(Math.random()*255);
  var b=Math.floor(Math.random()*255);

  calcVerticeOrder([v0x,v0y], [v1x,v1y], [v2x,v2y], [r,g,b]);

}

function calcVerticeOrder(a, b, c, color){

	var temp = [0,0];

	if (a[1] < c[1]){
		temp = a;
		a = c;
		c = temp;	
	}
	
	if (a[1] < b[1]){
		temp = a;
		a = b;
		b = temp;
	}
	
	if (b[1] < c[1]){
		temp = b;
		b = c;
		c = temp;	
	}
	
	drawTriangle(a, b, c, color);
}

//
// EOF
//
